﻿using Extension.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Extension.Ext;
using System.Collections.Generic;

namespace ExtensionTests.Lambda
{
    [TestClass]
    public class LambdaTest
    {
        #region Initializations
        private List<Produit> _produits;
        private List<Produit> _produits2;
        private List<string> joursSemaine;

        private Person _person = new Person("Lescop", "Celine");
        private Produit _produit = new Produit(1,"Nintendo Switch",300,100);


        // les produits
        Produit p1 = new Produit(1, "Smartphone", 300, 7);
        Produit p2 = new Produit(2, "Ordinateur", 750, 10);
        Produit p3 = new Produit(2, "Ordinateur", 750, 10);
        Produit p4 = new Produit(4, "Scanner", 70, 21);

        #endregion

        #region Tests du TP4

        [TestInitialize]
        public void Initialize()
        {
            // produits
            _produits = new List<Produit>();
            _produits.Add(new Produit(1, "Smartphone", 300, 7));
            _produits.Add(new Produit(2, "Ordinateur", 750, 10));
            _produits.Add(new Produit(2, "Ordinateur", 750, 10));
            _produits.Add(new Produit(4, "Scanner", 70, 21));

            _produits2 = new List<Produit>();
            _produits2.Add(p1);
            _produits2.Add(p2);
            _produits2.Add(p3);
            _produits2.Add(p4);

            // jours de la semaine
            joursSemaine = new List<string>
            {
                "lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"
            };
        }
        #region Test du nom de famille
        [TestMethod]
        public void TestGetLastName()
        {
            //même chose avec une lambda
            // lambda définie par :
            // - son type de données : Func<>
            // - son nom             : getLastName
            // - son code            : () => ...
            Func<Person, string> getLastname = (p) => p.Lastname;
            
            //getLastname est une fonction et veut un paramètre
            Assert.AreEqual("Lescop", getLastname(_person));
            
        }

        #endregion

        #region Test du prénom
        [TestMethod]
        public void TestGetFirstName()
        {
            Func<Person, string> getFirstName = p => p.Firstname;
            Assert.AreEqual("Celine", getFirstName(_person));
        }

        #endregion

        #region Tests du nom, prénoms, concaténation et concaténation en nom Maj
        [TestMethod]
        public void TestGetString()
        {
            //test du nom
            Func<Person, string> getString = p => p.Lastname;
            Assert.AreEqual("Lescop", getString(_person));

            //test du prénom
            getString = p => p.Firstname;
            Assert.AreEqual("Celine", getString(_person));

            //concatenation du nom et prénom
            getString = p => p.Lastname + " " + p.Firstname;
            Assert.AreEqual("Lescop Celine", getString(_person));

            //concatenation du nom et prenom avec nom en Upper
            getString = p => p.Lastname.ToUpper() + " " + p.Firstname;
            Assert.AreEqual("LESCOP Celine", getString(_person));
        }

        #endregion

        #region Tests du prix, produit et la valeur du stock
        [TestMethod]
        public void getProduit()
        {
            Func<Produit, double> getDouble = p => p.Prix;
            Assert.AreEqual(300, getDouble(_produit));

            getDouble = p => p.Stock;
            Assert.AreEqual(100, getDouble(_produit));

            // valeur du stock
            getDouble = p => p.Prix * p.Stock;
            Assert.AreEqual(30000, getDouble(_produit));

        }

        #endregion

        #region Test de renvoie d'infos générales
        [TestMethod]
        public void TestGetInformation()
        {
            //Assert.AreEqual("Lescop", _person.getInformation(_person => _person.Lastname));
            //Assert.AreEqual(Convert.ToString(300), _produit.getInformation(_produit => _produit.Prix.ToString()));
        }

        #endregion

        #region Test du filtre lambda
        [TestMethod]
        public void TestFiltreListLambda()
        {
            var expectedList = new List<Produit>
            {
                p1,p2,p3
            };

            var actual = _produits2.FilterListLambda(
                p => p.Prix > 200);

            CollectionAssert.AreEqual(expectedList, actual);
        }

        [TestMethod]
        public void TestFiltreListLambdaJoursSemaine()
        {
            var expectedList = new List<string>
            {
                "mardi","mercredi"
            };

            var actual = joursSemaine.FilterListLambda(
                d => d.StartsWith("m"));

            CollectionAssert.AreEqual(expectedList, actual);
        }
        #endregion

        #endregion

    }
}
