﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Extension.Ext;

namespace ExtensionTests
{
    [TestClass]
    public class DateTimeExtensionTest
    {
        #region Tests DateTime du TP1

        [TestMethod]
        public void TestToMySQL()
        {
            var date = new DateTime(2021, 01, 21);
            Assert.AreEqual("2021-01-21", date.ToMySQL());
        }

        [TestMethod]
        public void TestIsWeekDay()
        {
            var date = new DateTime(2021, 01, 21);
            Assert.AreEqual(true, date.IsWeekDay());
        }

        [TestMethod]
        public void TestIsWeekEnd()
        {
            var date = new DateTime(2021, 01, 23);
            Assert.AreEqual(true, date.IsWeekEnd());
        }

        [TestMethod]
        public void TestBetween()
        {
            var date = new DateTime(2021, 01, 22);
            var date1 = new DateTime(2021, 01, 21);
            var date2 = new DateTime(2021, 01, 23);
            Assert.AreEqual(true, date.Between(date1, date2));
        }

        [TestMethod]
        public void TestBetween2()
        {
            var date = new DateTime(2021, 01, 22);
            var date1 = new DateTime(2021, 01, 21);
            var date2 = new DateTime(2021, 01, 23);
            Assert.AreEqual(true, date.Between(date1, date2));
        }

        [TestMethod]
        public void TestBissextile()
        {
            var date = new DateTime(2024, 01, 23);
            Assert.AreEqual(true, date.Bissextile());
        }
        #endregion
    }
}
