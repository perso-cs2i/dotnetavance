﻿using Extension.Model;
using Extension.Ext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ExtensionTests
{
    [TestClass]
    public class PersonTest
    {
        [TestMethod]
        public void TestFirstName()
        {
            var p = new Person("Lescop", "Celine");
            Assert.AreEqual("Celine", p.Firstname);
        }

        [TestMethod]
        public void TestLastName()
        {
            var p = new Person("Lescop", "Céline");
            Assert.AreEqual("Lescop", p.Lastname);
        }

        [TestMethod]
        public void TestToString()
        {
            var p = new Person("Lescop", "Céline");
            Assert.AreEqual("Lescop Céline", p.ToString());
        }

        [TestMethod]
        public void TestToStringInverse()
        {
            var p = new Person("Lescop", "Céline");
            Assert.AreEqual("Céline Lescop", PersonBis.ToStringInverse(p));
        }

        [TestMethod]
        public void TestFirstNameLastName()
        {
            var p = new Person("Lescop","Céline");
            Assert.AreEqual(
                "Céline Lescop",
                p.FirstNameLastName());
        }
    }
}
