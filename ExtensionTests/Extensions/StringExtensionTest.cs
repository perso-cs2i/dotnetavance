﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Extension.Ext;

namespace ExtensionTests
{
    [TestClass]
    public class StringExtensionTest
    {
        #region Tests String du TP1

        [TestMethod]
        public void TestLeft()
        {
            var mot = "Delta";
            Assert.AreEqual("Del", mot.Left(3));
        }

        [TestMethod]
        public void TestRight()
        {
            var mot = "Delta";
            Assert.AreEqual("ta", mot.Right(2)); 
        }

        [TestMethod]
        public void TestFillLeft()
        {
            var word = "Alpha";
            Assert.AreEqual("Alph", word.FillLeft(4));
        }

        [TestMethod]
        public void TestFillLeft2()
        {
            var word = "Alpha";
            Assert.AreEqual("####Alpha", word.FillLeft(9, "#"));
        }

        [TestMethod]
        public void TestFillRight()
        {
            var word = "Alpha";
            Assert.AreEqual("pha", word.FillRight(3));
        }

        [TestMethod]
        public void TestFillRight2()
        {
            var word = "Alpha";
            Assert.AreEqual("Alpha****", word.FillRight(9, "*"));
        }

        [TestMethod]
        public void AddSuffix1()
        {
            var word = "Alpha";
            Assert.AreEqual("AlphaTest", word.AddSuffix("Test"));
        }

        [TestMethod]
        public void AddSuffix2()
        {
            var word = "AlphaTest"; 
            Assert.AreEqual("AlphaTest", word.AddSuffix("Test"));
        }

        [TestMethod]
        public void AddSuffix3()
        {
            var word = "alphatest";
            Assert.AreEqual("alphaTest", word.AddSuffix("Test"));
        }

        [TestMethod]
        public void Ellipsis1()
        {
            var word = "Alpha Bravo";
            Assert.AreEqual("Alpha...", word.Ellipsis(5));
        }

        [TestMethod]
        public void Ellipsis2()
        {
            var word = "Alpha Bravo";
            Assert.AreEqual("Alpha++", word.Ellipsis(5,"++"));
        }

        [TestMethod]
        public void Ellipsis3()
        {
            var word = "Alpha Bravo";
            Assert.AreEqual("Alpha Bravo", word.Ellipsis(11));
        }

        [TestMethod]
        public void Ellipsis4()
        {
            var word = "Alpha Bravo";
            Assert.AreEqual("Alpha Bravo", word.Ellipsis(20));
        }

        [TestMethod]
        public void CleanPath()
        {
            var s = @"L|u\n.d/+i";
            Assert.AreEqual(@"Lun.d+i", s.CleanPath());
        }
        #endregion
    }
}
