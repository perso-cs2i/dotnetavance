﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Extension.Ext;
using System.Collections.Generic;
using Extension.Model;

namespace ExtensionTests
{
    [TestClass]
    public class ProduitExtensionTest
    {
        #region Initializations
        private List<Produit> _produits;

        [TestInitialize]
        public void Initialize()
        {
            _produits = new List<Produit>();
            _produits.Add(new Produit(1, "Smartphone", 300, 7));
            _produits.Add(new Produit(2, "Ordinateur", 750, 10));
            _produits.Add(new Produit(3, "Imprimante", 150, 4));
            _produits.Add(new Produit(4, "Scanner", 70, 21));
        }

        #endregion

        #region Test du prix maximum
        [TestMethod]
        public void TestPrixMax()
        {
            Assert.AreEqual(750, _produits.PrixMax());
        }

        #endregion

        #region Test du prix minimum
        [TestMethod]
        public void TestPrixMin()
        {
            Assert.AreEqual(70, _produits.PrixMin());
        }

        #endregion

        #region Test du prix moyen
        [TestMethod]
        public void TestPrixMoyen()
        {
            Assert.AreEqual(317.5, _produits.PrixMoyen());
        }

        #endregion

        #region Test sur la quantité totale
        [TestMethod]
        public void TestQuantiteTotale()
        {
            Assert.AreEqual(42, _produits.QuantiteTotale());
        }

        #endregion

        #region Test de la valeur totale avec les stocks
        [TestMethod]
        public void TestValeurStock()
        {
            Assert.AreEqual(11670, _produits.ValeurStock());
        }

        #endregion
        
    }
}
