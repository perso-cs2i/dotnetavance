﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Extension.Ext;
using System.Collections.Generic;
using System.Linq;

namespace ExtensionTests
{
    [TestClass]
    public class ListExtensionTest
    {
        #region Initializations

        private List<string> joursSemaine;
        [TestInitialize]
        public void Initialize()
        {
            joursSemaine = new List<string>
            {
                "lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"
            };
        }

        #endregion

        #region Tests Lists du TP2

        #region Lower

        [TestMethod]
        public void TestLower()
        {
            var list = new List<string>
            {
                "TEst",
                "DOTneT"
            };

            var expectedList = new List<string>
            {
                "test",
                "dotnet"
            };

            var resultList = list.ToLower();

            CollectionAssert.AreEqual(
                expectedList, resultList);
            
        }
        #endregion

        #region Upper
        [TestMethod]
        public void TestUpper()
        {
            var list = new List<string>
            {
                "TEst",
                "DOTneT"
            };

            var expectedList = new List<string>
            {
                "TEST",
                "DOTNET"
            };

            var resultList = list.ToUpper();

            CollectionAssert.AreEqual(
                expectedList, resultList);

        }

        #endregion

        #region ValeurMax 
        [TestMethod]
        public void TestMaxValue()
        {
            var list = new List<int>
            {
                5,
                10, 
                1
            };
 
            var resultList = list.MaxValue();
            Assert.AreEqual(10, resultList);
        }

        #endregion

        #region ValeurMin
        [TestMethod]
        public void TestMinValue()
        {
            var list = new List<int>
            {
                5,
                10,
                1
            };

            var resultList = list.MinValue();
            Assert.AreEqual(1, resultList);
        }

        #endregion

        #region Somme de la liste en Int
        [TestMethod]
        public void TestListTotal()
        {
            var list = new List<int>
            {
                5,
                10,
                1
            };

            var resultList = list.ListTotal();
            Assert.AreEqual(16, resultList);
        }

        #endregion

        #region Moyenne de la liste en Int
        [TestMethod]
        public void TestListAverage()
        {
            var list = new List<double>
            {
                5,
                10,
                1
            };

            double w = (double)16/3;
            double x = Math.Round(w, 2);

            var resultList = list.ListAverage();
            Assert.AreEqual(x, resultList);
        }

        #endregion

        #region Cherche la chaine la plus longue
        [TestMethod]
        public void TestLonguestString()
        {
            var list = new List<string>
            {
                "test",
                "dotnet",
                "ok"
            };

            var resultList = list.LonguestString();
            Assert.AreEqual("dotnet", resultList);
        }

        #endregion

        #region Cherche la chaine la plus petite
        [TestMethod]
        public void TestShortestString()
        {
            var list = new List<string>
            {
                "test",
                "dotnet",
                "ok"
            };

            var resultList = list.ShortestString();
            Assert.AreEqual("ok", resultList);
        }

        #endregion

        #region Filtre une liste
        [TestMethod]
        public void TestFilterList()
        { 
            var expectedList = new List<string>
            {
                "mercredi",
                "vendredi"
            };

            var resultList = joursSemaine.FilterList("re");
            CollectionAssert.AreEqual(expectedList, resultList);
        }

        #endregion

        #region Filtrer advanced une liste
        [TestMethod]
        public void TestFilterListAdvancedStart()
        {
            var expectedList = new List<string>
            {
                "mardi",
                "mercredi"
            };

            var expectedList2 = new List<string>
            {
                "samedi"
            };

            // LIKE 'm%'
            CollectionAssert.AreEqual(
                expectedList,
                joursSemaine.FilterListAdvanced("m", SearchPosition.Start));

            // LIKE 's%'
            CollectionAssert.AreEqual(
                expectedList2,
                joursSemaine.FilterListAdvanced("s", SearchPosition.Start));
        }

        [TestMethod]
        public void TestFilterListAdvancedMiddle()
        {
            var expectedList = new List<string>
            {
                "mercredi",
                "vendredi"
            };

            var expectedList2 = new List<string>
            {
                "dimanche"
            };

            // LIKE 'm%'
            CollectionAssert.AreEqual(
                expectedList,
                joursSemaine.FilterListAdvanced("re", SearchPosition.Middle));

            // LIKE 's%'
            CollectionAssert.AreEqual(
                expectedList2,
                joursSemaine.FilterListAdvanced("che", SearchPosition.Middle));
        }

        [TestMethod]
        public void TestFilterListAdvancedEnd()
        {
            var expectedList = new List<string>
            {
                "lundi",
                "mardi",
                "mercredi",
                "jeudi",
                "vendredi",
                "samedi"
            };

            var expectedList2 = new List<string>
            {
                "dimanche"
            };

            // LIKE 'm%'
            CollectionAssert.AreEqual(
                expectedList,
                joursSemaine.FilterListAdvanced("i", SearchPosition.End));

            // LIKE 's%'
            CollectionAssert.AreEqual(
                expectedList2,
                joursSemaine.FilterListAdvanced("e", SearchPosition.End));
        }

        #endregion

        #region Affichage d'une liste par nombre de lettres
        [TestMethod]
        public void TestJoursCinq()
        {

            var expectedList = new List<string>
            {
                "lundi",
                "mardi",
                "jeudi"
            };

            // jours qui ont 5 lettres
            CollectionAssert.AreEqual(
                expectedList,
                joursSemaine.FilterLength(5)
                );
        }

        [TestMethod]
        public void TestJoursHuit()
        {
            var expectedList = new List<string>
            {
                "mercredi",
                "vendredi",
                "dimanche"
            };

            // jours qui ont 5 lettres
            CollectionAssert.AreEqual(
                expectedList,
                joursSemaine.FilterLength(8)
                );
        }

        #endregion

        #region Limit
        [TestMethod]
        public void TestLimit2()
        {
            var list1 = new List<string> { "monday", "tuesday", "wednesday" };
            var result1 = new List<string> { "monday", "tuesday" };

            var list2 = new List<int> { 10, -3, 3, 12};
            var result2 = new List<int> { 10 };

            CollectionAssert.AreEqual(result1, list1.Limit2(2).ToList());
            CollectionAssert.AreEqual(result2, list2.Limit2(1).ToList());
           
        }

        #endregion
        
    }
    #endregion
}
