﻿using Extension.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Ext
{
    
    public static class ProduitExtension
    {
        public static int PrixMax(this List<Produit> sourceList)
        {
            var max = sourceList[0].Prix;

            foreach (var item in sourceList)
            {
                if (item.Prix >= max)
                {
                    max = item.Prix;
                }
            }
            return max;
        }

        public static int PrixMin(this List<Produit> sourceList)
        {
            var min = sourceList[0].Prix;

            foreach (var item in sourceList)
            {
                if (item.Prix <= min)
                {
                    min = item.Prix;
                }
            }
            return min;
        }

        public static double PrixMoyen(this List<Produit> sourceList)
        {
            double moyenne = 0;
            double moy = 0;
            double res = 0;

            foreach (var item in sourceList)
            {
                    res = res + item.Prix;  
            }
            moyenne = res / sourceList.Count;
            moy = Math.Round(moyenne, 2);

            return moy;
        }

        public static int QuantiteTotale(this List<Produit> sourceList)
        {
            var total = 0;

            foreach (var item in sourceList)
            {
                total = total + item.Stock;
            }

            return total;
        }

        public static int ValeurStock(this List<Produit> sourceList)
        {
            var total = 0;
            foreach (var item in sourceList)
            {
                total = total + item.Prix * item.Stock;
            }
            return total;
        }
    }
}
