﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Ext
{
    public static class Lambda
    {
        // je passe un type générique avec une varriable : element quelconque : Produit ou Person, puis une fonction d'un objet quelconque : Produit ou Person

        // this T item : on étend n'importe quel type .NET (d'où T)
        // getInfo     : une fonction lambda qui s'appliquera aux objets de type T
        //               et qui en entraira une chaine
        //               Dans la signature de la méthode d'extension, getInfo
        //               est une varaibel (de type lambda)
        public static string getInformation<T>(this T element, Func<T, string> getInfo) 
        {
            // on utilise la variable getInfo comme fonction, pour pouvoir l'appeler
            return getInfo(element);
        }
        
    }
}
