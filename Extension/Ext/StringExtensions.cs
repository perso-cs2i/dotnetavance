﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Ext
{
    public static class StringExtensions
    {
        /// <summary>
        /// Take 3 left letters of the word
        /// </summary>
        /// <param name="mot">The word</param>
        /// <returns>New word with 3 left letters</returns>
        public static string Left(this string mot, int length)
        {
            //var a = mot.Length;
            return mot.Substring(0,length);
        }

        /// <summary>
        /// Take 2 right letters of the word
        /// </summary>
        /// <param name="mot">The word</param>
        /// <returns>New word with 2 right letters</returns>
        public static string Right(this string mot, int length)
        {
            int v = mot.Length - length;
            return mot.Substring(v,length);
        }

        /// <summary>
        /// Take the word cut or with some caracters whitch choose
        /// </summary>
        /// <param name="word">The word</param>
        /// <param name="caracter">Caracter who choose</param>
        /// <returns>The word or the caracter+word</returns>
        public static string FillLeft(this string word, int length, string caracter = " ")
        {
            if (length < word.Length) // 4 < 5
            {
                return word.Substring(0, length);
            }
            else
            {
                int enPlus = length - word.Length; // 7-5 = 2
                string ncaracter = "";
                int i;
                for (i = 0; i < enPlus; i++)
                {
                    ncaracter = ncaracter + "" + caracter;
                }
                return ncaracter + "" + word;
            }
        }

        /// <summary>
        /// Take the word cut or with some caracters whitch choose
        /// </summary>
        /// <param name="word">The word</param>
        /// <param name="caracter">Caracter who choose</param>
        /// <returns>The word or the word+caracter</returns>
        public static string FillRight(this string word, int length, string caracter = " ")
        {
            if (length < word.Length) // 3 < 5
            {
                int d = word.Length - length; // 5 - 3 = 2
                return word.Substring(d, length);
            }
            else
            {
                int enPlus = length - word.Length; // 9 - 5 = 4
                string ncaracter = "";
                int i;
                for (i = 0; i < enPlus; i++)
                {
                    ncaracter = ncaracter + "" + caracter;
                }
                return word + "" + ncaracter;
            }
        }

        /// <summary>
        /// Traitement on suffix and word
        /// </summary>
        /// <param name="word">The word</param>
        /// <param name="suffix">The suffix</param>
        /// <returns>The word with the suffix</returns>
        public static string AddSuffix(this string word, string suffix)
        {
            string suffixMaj = suffix.ToUpper(); // "test" => "TEST"
            var a = word.Length; // taille du mot "alphatest" => 9
            var taille = suffixMaj.Length; // 4
            var t = a - taille; // 5
            string nword = word.Right(taille); // test
            string nwordMaj = nword.ToUpper(); // TEST
            string wordMaj = word.ToUpper();

            if (wordMaj.Contains(suffixMaj))
            {
                if(nwordMaj == suffixMaj) // TEST => TEST
                {
                    string fword = word.Left(t);
                    return fword + suffix;
                }
                else
                {
                    return word + suffix; // ne sert à rien 
                }
            }
            else
            {
                return word + suffix;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="word">The word</param>
        /// <param name="length">The size</param>
        /// <param name="caracter">The caracter</param>
        /// <returns>Word with caracters or not</returns>
        public static string Ellipsis(this string word, int length, string caracter = "...")
        {
            if (word.Length > length) // 11 > 5
            {
                var nword = word.Left(length); 
                return nword + caracter;
            }
            else
            {
                return word;
            }
        }

        public static string CleanPath(this string word)
        {

            var res = word.Replace("?", "")
                .Replace("|", "")
                .Replace("/", "")
                .Replace(@"\", "");
            return res;

        }
    }
}
