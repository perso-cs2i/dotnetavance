﻿using Extension.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Ext
{
    /// <summary>
    /// Extension methods for class Person
    /// </summary>
    public static class PersonExtension
    {
        public static string FirstNameLastName(this Person p)
        {
            return $"{p.Firstname} {p.Lastname}";
        }
    }
}
