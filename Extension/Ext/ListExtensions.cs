﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Ext
{
    public enum SearchPosition
    {
        Start,
        Middle,
        End
    } 

    public static class ListExtensions
    {
        public static List<string> ToLower(this List<string> sourceList)
        {
            var resultList = new List<string>();
            foreach (var item in sourceList)
            {
                resultList.Add(item.ToLower());
            }
            return resultList;
        }

        public static List<string> ToUpper(this List<string> sourceList)
        {
            var resultList = new List<string>();
            foreach (var item in sourceList)
            {
                resultList.Add(item.ToUpper());
            }
            return resultList;
        }

        public static int MaxValue(this List<int> sourceList)
        {
            var max = sourceList[0];

            foreach (var item in sourceList)
            {
                if(item >= max)
                {
                    max = item;
                }  
            }
             return max;       
        }

        public static int MinValue(this List<int> sourceList)
        {
            var min = sourceList[0];

            foreach (var item in sourceList)
            {
                if (item <= min)
                {
                    min = item;
                }

            }
            return min;
        }

        public static int ListTotal(this List<int> sourceList)
        {
            var res = 0;

            foreach (var item in sourceList)
            {
                res = item + res;
            }

            return res;
        }

        public static double ListAverage(this List<double> sourceList)
        {
            double moyenne = 0;
            double moy = 0;
            double res = 0;

            foreach (var item in sourceList)
            {
                res = item + res;
            }

            moyenne = res / sourceList.Count;
            moy = Math.Round(moyenne, 2); 

            return moy;
        }

        public static string LonguestString(this List<string> sourceList)
        {
            var plusGrandeChaine = sourceList[0];
            foreach (var item in sourceList)
            {
                if(item.Length > plusGrandeChaine.Length)
                {
                    plusGrandeChaine = item;
                }
            }

            return plusGrandeChaine;
        }

        public static string ShortestString(this List<string> sourceList)
        {
            var plusPetiteChaine = sourceList[0];
            foreach (var item in sourceList)
            {
                if (item.Length < plusPetiteChaine.Length)
                {
                    plusPetiteChaine = item;
                }
            }

            return plusPetiteChaine;
        }

        public static List<string> FilterList(this List<string> sourceList, string chaine ="")
        {
            var resultList = new List<string>();
            foreach (var item in sourceList)
            {
                if (item.Contains(chaine))
                {
                    resultList.Add(item);
                }
            }
            return resultList;
        }

        public static List<string> FilterListAdvanced(
            this List<string> sourceList, 
            string recherche,
            SearchPosition recherchePos)
        {
            var resultList = new List<string>();
            
            switch (recherchePos)
            {
                case SearchPosition.Start:
                    foreach(var item in sourceList)
                    {
                        if (item.StartsWith(recherche))
                        {
                            resultList.Add(item);
                        }

                    }
                    break;

                case SearchPosition.Middle:
                    foreach (var item in sourceList)
                    {
                        if (item.Contains(recherche))
                        {
                            resultList.Add(item);
                        }

                    }
                    break;

                case SearchPosition.End:
                    foreach (var item in sourceList)
                    {
                        if (item.EndsWith(recherche))
                        {
                            resultList.Add(item);
                        }
                    }
                    break;
            }
            return resultList;
        }

        public static List<T> FilterListLambda<T>(this List<T> sourceList, Func<T, bool> filtre)
        {
            var resultList = new List<T>();
            // parcours de la collection, et conserver ceux qui répondent au filtre
            foreach (T item in sourceList)
            {
                if (filtre(item))
                {
                    resultList.Add(item);
                }
            }

            return resultList;
        }
        //public static List<T> FilterListLambda<T>(this List<T> items, Func<List<T>, T> Filter)
        
        public static string getInformation<T>(this T element, Func<T, string> getInfo)
        {
           // on utilise la variable getInfo comme fonction, pour pouvoir l'appeler
           return getInfo(element);
        }

        public static List<string> FilterLength(this List<string> sourceList,int numLetters)       
        {
            var resultList = new List<string>();

            foreach (var item in sourceList)
            {
                if (item.Length == numLetters)
                {
                    resultList.Add(item);
                }
            
            }
            return resultList;
        }

        /// <summary>
        /// Extract n items from a list
        /// </summary>
        /// <typeparam name="T">The type of the items</typeparam>
        /// <param name="sourceList">List of items of type T</param>
        /// <returns></returns>
        public static List<T> Limit<T>(this List<T> sourceList, int maxItems)
        {
            var resultList = new List<T>();
            var i = 0;

            foreach (var item in sourceList)
            {
                if (i > maxItems) break;

                resultList.Add(item);
                i++;
            }

            return resultList;
        }

        public static List<T> Limit2<T>(this List<T> sourceList, int maxItems)
        {
            var resultList = new List<T>();
 
            for(var i = 0; i < maxItems; i++)
            {
                resultList.Add(sourceList[i]);
            }

            return resultList;
        }

        public static IEnumerable<T> Limit3<T>(this IEnumerable<T> sourceList, int maxItems)
        {
            return null;
        }

    }
}
