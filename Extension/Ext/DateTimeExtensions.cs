﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Ext
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// DateTime in a specific format
        /// </summary>
        /// <param name="d">The DateTime</param>
        /// <returns>DateTime d in format : yyyy-MM-dd</returns>
        public static string ToMySQL(this DateTime d)
        {      
            string inter = d.ToString("yyyy-MM-dd");
            return inter;
        }

        /// <summary>
        /// if the date is a week day or not
        /// </summary>
        /// <param name="d">The DateTime</param>
        /// <returns>True</returns>
        public static bool IsWeekDay(this DateTime d)
        {
            if (d.DayOfWeek != DayOfWeek.Saturday && d.DayOfWeek != DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                return false;
            }
                
        }

        /// <summary>
        /// if the date is a week end or not
        /// </summary>
        /// <param name="d">The DateTime</param>
        /// <returns>True</returns>
        public static bool IsWeekEnd(this DateTime d)
        {
            if (d.DayOfWeek == DayOfWeek.Saturday || d.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                return false;
            }

            
        }

        
        public static bool Between(this DateTime d, DateTime date1, DateTime date2)
        {
            // 1er test avec la première date
            int res1 = DateTime.Compare(date1, d);
            string resultat1 = "";
            if (res1 < 0)
            {
                resultat1 = "earlier";
            }
            else if (res1 == 0)
            {
                resultat1 = "same";
            } 
            else
            {
                resultat1 = "later";
            }

            // 2ème test avec la deuxième date
            int res2 = DateTime.Compare(date2, d);
            string resultat2 = "";
            if (res2 < 0)
            {
                resultat2 = "earlier";
            }
            else if (res2 == 0)
            {
                resultat2 = "same";
            }
            else
            {
                resultat2 = "later";
            }

            // verif
            if (resultat1 == "earlier" && resultat2 == "later")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool Between2(this DateTime d, DateTime date1, DateTime date2)
        {
            return date1 < d && d < date2;
        }

        public static bool Bissextile(this DateTime d)
        {
            var dateInter = d.ToMySQL(); // "yyyy-MM-dd"
            var yearInter = dateInter.Left(4); // 2024

            //conversion de l'année en int
            int annee = int.Parse(yearInter);

            // tests 
            if(annee.IsDivisibleBy(4) == true && annee.IsDivisibleBy(100) == false) // divisble par 4 et pas par 100 donc bissextile
            {
                return true;
            }
            else if (annee.IsDivisibleBy(400) == true) // divisible par 400 donc bissextile
            {
                return true;
            }
            else // ni l'un ni l'autre donc pas bissextile
            {
                return false;
            }
        }
    }
}
