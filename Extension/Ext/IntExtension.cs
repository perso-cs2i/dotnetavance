﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Ext
{
    public static class IntExtension
    {
        /// <summary>
        /// Check if an integer is even
        /// </summary>
        /// <param name="i">The number</param>
        /// <returns>True if the number is even</returns>
        public static bool isEven(this int i)
        {
            return i % 2 == 0;  
        }

        /// <summary>
        /// Check if an integer is even
        /// </summary>
        /// <param name="i">The number</param>
        /// <returns>True if the number is odd</returns>
        public static bool isOdd(this int i)
        {
            return i % 2 != 0;
        }

        /// <summary>
        /// Check if a number is divisble by another number (the divider)
        /// </summary>
        /// <param name="i">The number</param>
        /// <param name="divider">The divider</param>
        /// <returns>True if the number if divisible by the divider</returns>
        public static bool IsDivisibleBy(this int i, int divider)
        {
            return i % divider == 0;
        }
    }
}
