﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Model
{
    public static class PersonBis
    {
        public static string ToStringInverse(Person p)
        {
            return $"{p.Firstname} {p.Lastname}";
        }
    }
}
