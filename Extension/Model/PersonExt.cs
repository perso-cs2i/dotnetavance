﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Model
{
    public class PersonExt : Person
    {
        public PersonExt(string lastname, string firstname)
            : base(lastname,firstname) // base est l'héritage de la classe Person
        {
        }

        /// <summary>
        /// Méthode qui va inverser et afficher le prénom puis le nom
        /// </summary>
        /// <returns>Le prénom suivi du nom</returns>
        public string ToStringInverse()
        {
            return $"{Firstname} {Lastname}";
        }
    }
}
