﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Model
{
    /// <summary>
    /// Classe Personne
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="lastname">Last name of the person</param>
        /// <param name="firstname">First name of the person</param>
        public Person(string lastname, string firstname)
        {
            Lastname = lastname;
            Firstname = firstname;
        }

        /// <summary>
        /// getters et setters .NET simplifiés
        /// </summary>
        public string Lastname { get; set; }
        public string Firstname { get; set; }

        public override string ToString()
        {
            return $"{Lastname} { Firstname}"; //base c'est super en Java et parrent en PHP
        }
    }
}
