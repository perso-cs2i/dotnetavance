﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension.Model
{
    public class Produit
    {
        public Produit(int id, string designation, int prix, int stock)
        {
            Id = id;
            Designation = designation;
            Prix = prix;
            Stock = stock;
        }

        /// <summary>
        /// getters et setters .NET simplifiés
        /// </summary>
        public int Id { get; set; }
        public string Designation { get; set; }
        public int Prix { get; set; }
        public int Stock { get; set; }

    }
}
